/**
 * Help us remember the current session
 */
var progressModule = {
  session: null,
  playing: false,
  saveProgress: function (player, enemies) {
    if (player) {
      const hitsHistory = enemies.map(enemy => {
        return enemy.hit
      })
      const body = Object.assign(player, {enemies: [... hitsHistory]})
      this.session = JSON.stringify(body)
      sessionStorage.setItem('currentSession', this.session)
    } else {
      sessionStorage.setItem('currentSession', null)
    }
  },
  loadProgress: function () {
    this.session = JSON.parse(sessionStorage.getItem('currentSession'))
    const pieces = this.session ? Object.keys(this.session) : 0
    this.playing = pieces.length === 3
  }
}

export { progressModule }