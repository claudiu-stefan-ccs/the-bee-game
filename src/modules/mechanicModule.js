import { enemiesModule } from "./enemiesModule.js"
import { playerModule } from "./playerModule.js"
import { progressModule } from "./progressModule.js"
import bubbleTemplate from '../models/template.js'
import { sceneModule } from "./sceneModule.js"

/**
 * "Game mechanic" moldule, used to pick enemies at random, damage them and kill them.
 */
var mechanicModule = {
  fired: { index: -1, value: false },
  bindMechanic: function () {
    document.getElementById('button').addEventListener('click', this.fire.bind(this, enemiesModule.tmpEnemies))
  },
  /**
   * Function atriggered at click, hits a random enemy
   * @param {Array} enemies 
   */
  fire: function (enemies) {
    const deadEnemies = []
    enemies.forEach((enemy, index) => {
      if (!enemy.aliveStatus) {
        deadEnemies.push(index)
      }
    })
    const index = this.hipFire(enemies, deadEnemies)
    enemiesModule.tmpEnemies[index].hit++
    if (!enemiesModule.tmpEnemies[index].aliveStatus) {
      enemiesModule.tmpEnemies[index].src = '../assets/dead.png'
    }
    this.fired.value = true
    this.fired.index = index
    playerModule.hitCounter('mechanic')
    sceneModule.createBoard(enemiesModule.tmpEnemies)
    progressModule.saveProgress(playerModule.player, enemiesModule.tmpEnemies)
    this.hitDetection()
  },
  /**
   * helps us identify a dead enemy so we can exclude him from the roster
   * @param {Array} enemies 
   * @param {Array} deadEnemies 
   * @returns 
   */
  hipFire: function (enemies, deadEnemies) {
    const sporadic = Math.floor(Math.random() * enemies.length)
    if (deadEnemies.includes(sporadic)) {
      return this.hipFire(enemies, deadEnemies)
    } else {
      return sporadic
    }
  },
  /**
   * Visual helper for the enemy we hit
   */
  hitDetection: function () {
    const detected = document.getElementById(`enemy${this.fired.index}`)
    const detectedImg = document.getElementById(`img${this.fired.index}`)
    detected.classList.add('hit')
    detectedImg.classList.add('hit-animation')
    this.dmgBubble(detected, enemiesModule.tmpEnemies[this.fired.index].tickdmg)
    setTimeout(() => {
      detected.classList.remove('hit')
      detectedImg.classList.remove('hit-animation')
      sceneModule.endGame(enemiesModule.tmpEnemies)
    }, 500);
  },
  /**
   * Visual helper for the enemy we hit
   */
  dmgBubble: function (enemy, damage) {
    const bubble = document.createElement('div')
    bubble.innerHTML = bubbleTemplate.bubbleTemplate
    bubble.setAttribute('class', 'game-absolute')
    bubble.style.top = '35px'
    bubble.style.right = '0'
    bubble.style.zIndex = '9'
    enemy.appendChild(bubble)
    const hitDamage = document.getElementById('bubble')
    hitDamage.innerHTML = `${damage} DMG!`
    setTimeout(() => {
      bubble.innerHTML = ''
    }, 500);
  }, 
}

export {mechanicModule}