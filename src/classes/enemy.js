export default class Enemy {
  hit = 0
  constructor (totalHP, type, tickdmg, name, src) {
    this.totalHP = totalHP
    this.type = type
    this.tickdmg = tickdmg
    this.name = name
    this.src = src
  }
  get currentHP () {
    return this.getHpLeft()
  }
  get aliveStatus () {
    return this.getHpLeft() > 0
  }
  getHpLeft () {
    return this.totalHP - this.tickdmg * this.hit
  }
}
