export default class Player {
  constructor (name, hits) {
    this.name = name
    this.hits = hits
  }  
  get currentName () {
    return this.name
  }
}