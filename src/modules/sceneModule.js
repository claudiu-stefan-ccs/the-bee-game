import Player from '../classes/player.js'
import statusTemplate from '../models/template.js'
import loginTemplate from '../models/template.js'
import modalTemplate from '../models/template.js'
import { playerModule } from "../modules/playerModule.js"
import { progressModule } from "../modules/progressModule.js"
import { enemiesModule } from './enemiesModule.js'
import { mechanicModule } from './mechanicModule.js'

/**
 * Module used to deteremine which scene we're in: MENU or GAME
 */
var sceneModule = {
  loginScene: document.getElementById('login'),
  gameScene: document.getElementById('game'),
  stats: document.getElementById('stats'),
  modal: document.getElementById('modal'),
  button: '',
  /**
   * Function used to determine the layout of the current scene as well as populate the current scene
   * With the help of the progress status it helps us determine in which scene we should be
   * @param {Boolean} status 
   * @param {Array} rows 
   * @param {Array} enemies 
   * @param {Player} player 
   */
  currentScene: function (status, rows, enemies, player) {
    console.log(status)
    if (status) {
      this.clearScenes([this.loginScene])
      this.loginScene.parentElement.classList.remove('container')
      for (let i = 0; i < rows.length; i++) {
        let row = document.createElement('div')
        row.setAttribute('class', 'game-flex game-flow')
        row.setAttribute('id', `${rows[i].type}`)
        this.gameScene.appendChild(row)
      }
      this.createBoard(enemies)
      this.stats.classList.add('game-stats')
      this.stats.innerHTML = statusTemplate.statusTemplate
      this.button = document.createElement('button')
      this.button.setAttribute('id', 'button')
      this.button.innerHTML = 'Fire'
      this.gameScene.appendChild(this.button)
      mechanicModule.bindMechanic()
      playerModule.getPlayerInfo(player.name, player.hits)
      playerModule.getInfoLocation()
      setTimeout(() => {
        this.endGame(enemies)
      }, 500);
    } else {
      this.clearScenes([this.stats, this.gameScene])
      this.loginScene.parentElement.classList.add('container')
      this.loginScene.innerHTML = loginTemplate.loginTemplate
      this.button = document.getElementById('button')
      this.button.addEventListener('click', this.startGame.bind(this, 'login'))
    }
    this.modal.classList.remove('game-modal')
    this.clearScenes([this.modal])
  },
  /**
   * Creates the layout of the enemies that we should kill
   * @param {Array} enemies 
   */
  createBoard: function (enemies) {
    enemies.forEach((enemy, index) => {
      const erow = document.getElementById(`${enemy.type}`)
      const clear = document.getElementById(`enemy${index}`)
      if (clear && erow) {
        this.clearScenes([erow])
      }
      if (erow) {
        const enemyLocation = document.createElement('div')
        const currentHP = document.createElement('h2')
        const name = document.createElement('p')
        const img = document.createElement('img')
        enemyLocation.setAttribute('class', 'game-relative enemy')
        enemyLocation.setAttribute('id', `enemy${index}`)
        currentHP.setAttribute('id', `currentHP${index}`)
        name.innerHTML = `Name: ${enemy.name}`
        img.setAttribute('id', `img${index}`)
        img.setAttribute('src', `${enemy.src}`)
        if (enemy.aliveStatus) {
          currentHP.innerHTML =  `HP: ${enemy.currentHP}`
        } else {
          currentHP.innerHTML =  `${enemy.name} dead!`
        }
        erow.appendChild(enemyLocation)
        enemyLocation.appendChild(img)
        enemyLocation.appendChild(name)
        enemyLocation.appendChild(currentHP)
      }
    })
  },
  /**
   * Helps us start, restart a game or go back to menu
   * Reseting either some specs or all of them
   * @param {String} scene 
   * @returns 
   */
  startGame: function (scene) {
    switch (scene) {
      case 'login': 
        const input = document.getElementById('playerName')
        if (!input?.value.length) {
          alert('You must input a name!')
          return
        }
        progressModule.saveProgress({name: input.value, hits: 0}, enemiesModule.tmpEnemies)
        progressModule.playing = true
        this.currentScene(progressModule.playing, enemiesModule.enemies, enemiesModule.tmpEnemies, {name: input.value, hits: 0})
      break
      case 'ingame':
        playerModule.hitCounter()
        this.clearScenes([this.gameScene])
        progressModule.playing = true
        progressModule.saveProgress(playerModule.player, [])
        enemiesModule.clearEnemies()
        enemiesModule.createEnemies(progressModule.playing, [])
        this.currentScene(progressModule.playing, enemiesModule.enemies, enemiesModule.tmpEnemies, playerModule.player)
      break
      case 'restart':
        progressModule.playing = false
        progressModule.saveProgress(null, null)
        enemiesModule.clearEnemies()
        enemiesModule.createEnemies(progressModule.playing, [])
        this.currentScene(progressModule.playing)
      break
    }
  },
  /**
   * Determines if the game is over or not
   * @param {Array} enemies 
   */
  endGame: function (enemies) {
    const boss = enemies.find(enemy => enemy.type === 'boss')
    const deadEnemies = enemies.filter(enemy => !enemy.aliveStatus)
    if (!boss.aliveStatus || deadEnemies.length === enemies.length) {
      this.modal.classList.add('game-modal')
      this.modal.innerHTML = modalTemplate.modalTemplate
      document.getElementById('ingame').addEventListener('click', this.startGame.bind(this, 'ingame'))
      document.getElementById('restart').addEventListener('click', this.startGame.bind(this, 'restart'))
    }
  },
  /**
   * Helps us clean up the scene
   * @param {element} locations 
   */
  clearScenes: function (locations) {
    locations.forEach(location => {
      while(location.firstChild) {
        location.removeChild(location.firstChild)
      }
    })
  }
}

export { sceneModule }