import { enemiesModule } from "./src/modules/enemiesModule.js"
import { sceneModule } from "./src/modules/sceneModule.js"
import { progressModule } from "./src/modules/progressModule.js"
import { bees } from "./src/models/data.js"

window.onload = function () {
  if (!enemiesModule.enemies.length) {
    enemiesModule.getEnemies(bees)
  }
  progressModule.loadProgress()
  enemiesModule.createEnemies(progressModule.playing, progressModule.session?.enemies)
  sceneModule.currentScene(progressModule.playing, enemiesModule.enemies, enemiesModule.tmpEnemies, progressModule.session)
}