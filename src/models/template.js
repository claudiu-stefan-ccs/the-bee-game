var loginTemplate = `<div class="game-login">
<div class="field">
  <label for="player_name">Input your player: </label>
  <input type="text" name="player_name" id="playerName">
</div>
<div class="field">
  <button class="start" id="button">Start Game</button>
</div>
</div>`

var modalTemplate = `<div class="game-flex game-col modal-container">
<h1>Game over</h1>
<div class="game-flex game-col">
  <button class="start" id="ingame">
      Start New Game
  </button>
  <button class="start" id="restart">
      Back to Menu
  </button>
</div>
</div>`

var bubbleTemplate = `<div class="bubble" id="bubble"></div>`

var statusTemplate = `<p>Current player: <span id="username"></span></p>
<p>Current number of hits: <span id="hits"></span></p>`

export default {
  loginTemplate,
  modalTemplate,
  bubbleTemplate,
  statusTemplate
}