import Enemy from '../classes/enemy.js'

/**
 * Module used to init, create and refresh enemies
 */
var enemiesModule = {
  enemies: [],
  tmpEnemies: [],
  getEnemies: function (data) {
    this.enemies = data
  },
  createEnemies: function (status, data) {
    this.enemies.forEach(bee => {
      for(let i = 0; i < bee.value; i++) {
        this.tmpEnemies.push(new Enemy(bee.hp, bee.type, bee.tickdmg, bee.label, bee.src))
      }
    })
    if (status) {
      for (let i = 0; i < data.length; i++) {
        this.tmpEnemies[i].hit = data[i]
        if (!this.tmpEnemies[i].aliveStatus) {
          this.tmpEnemies[i].src = '../assets/dead.png'
        }
      }
    }
  },
  clearEnemies: function () {
    this.tmpEnemies = []
    this.tmpEnemies.length = 0
  }
}

export { enemiesModule }