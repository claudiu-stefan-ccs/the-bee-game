const bees = [
  { type: 'boss', hp: 100, tickdmg: 8, value: 1, label: 'Queen', src: '../assets/queen.gif' },
  { type: 'heavy', hp: 75, tickdmg: 10, value: 5, label: 'Worker', src: '../assets/worker.gif' },
  { type: 'minion', hp: 50, tickdmg: 12, value: 8, label: 'Drone', src: '../assets/drone.gif' }
]

export { bees }