import Player from '../classes/player.js'

/**
 * Module used for player creation and visualization
 */
var playerModule = {
  player: '',
  nameLocation: '',
  hitsLocation: '',
  getPlayerInfo: function (name, value) {
    this.player = new Player(name, value)
  },
  getInfoLocation: function () {
    setTimeout(() => {
      this.nameLocation = document.getElementById('username')
      this.hitsLocation = document.getElementById('hits')
      this.nameLocation.innerHTML = this.player.name
      this.hitsLocation.innerHTML = this.player.hits
    }, 0);
  },
  /**
   * Helps us keep tab on how many shots we took
   * @param {Any} value 
   */
  hitCounter (value) {
    this.player.hits = value ? this.player.hits + 1 : 0
    this.hitsLocation.innerHTML = this.player.hits
  }
}

export { playerModule }